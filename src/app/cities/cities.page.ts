import { Component } from '@angular/core';
import { WeatherService } from '../weather.service';
import { Storage } from '@ionic/storage';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-cities',
  templateUrl: 'cities.page.html',
  styleUrls: ['cities.page.scss']
})
export class CitiesPage {
  cities = [
    {name: 'London'},
    {name: 'Paris'},
    {name: 'La Plata'},
    {name: 'Cancun'},
    {name: 'Tokio'},
    {name: 'Hong Kong'},
    {name: 'Dublin'},
    {name: 'Shanghai'}
  ];
  
  STORAGE_KEY = 'favoriteCities';
  current_city:String;
  current_temp:DoubleRange;
  current_max:DoubleRange;
  current_min:DoubleRange;

  constructor(private weatherService: WeatherService,
              private storage: Storage,
              private alertController: AlertController) {}

  ngOnInit(){
  }

  async saveCity(city: String) {
    let result_1 = await this.getAllCities();
    if (result_1 == null) result_1 = [];
    if (result_1.indexOf(city) == -1) {
      result_1.push(city);
      this.storage.set(this.STORAGE_KEY, result_1);
      const alert = await this.alertController.create({
        header: 'Ciudad añadida a favoritos',
        buttons: ['OK']
      });
      await alert.present();
    }
    else {
      const alert = await this.alertController.create({
        header: 'Ciudad ya añadida',
        buttons: ['OK']
      });
      await alert.present();
    }
  }

  showCity(city:String){
    return this.weatherService.getWeather(city).subscribe(data => {
      this.current_temp = data['main']['temp'];
      this.current_max = data['main']['temp_max'];
      this.current_min = data['main']['temp_min'];
      this.current_city = city;
    });
  }

  async isFavorite(city: String) {
    const result_1 = await this.getAllCities();
    return result_1 && result_1.indexOf(city) !== -1;
  }

  getAllCities() {
    return this.storage.get(this.STORAGE_KEY);
  }
}
