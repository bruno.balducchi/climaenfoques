import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { WeatherService } from '../weather.service';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-favorites',
  templateUrl: 'favorites.page.html',
  styleUrls: ['favorites.page.scss']
})
export class FavoritesPage {
  
  STORAGE_KEY = 'favoriteCities';
  cities_data=[];

  constructor(private weatherService: WeatherService, private storage: Storage, private socialShare: SocialSharing) {}
  
  ionViewWillEnter(){
    this.cities_data = [];
    this.getFavorites();
  }

  getFavorites(){
    this.storage.get(this.STORAGE_KEY).then((val) => {
      val.forEach(city => {
        this.weatherService.getWeather(city).subscribe(data => {
          this.cities_data.push(
            {
              name: city,
              temp: data['main']['temp'],
              temp_max: data['main']['temp_max'],
              temp_min: data['main']['temp_min']
            }
          )
        });
      });
    });
  }

  haveFavorites () {
    return (this.cities_data.length > 0);
  }

  async removeFavorite(city) {
    const result_1 = await this.getAllCities();
    console.log (this.cities_data);
    if (result_1) {
      var index = result_1.indexOf(city.name);
      result_1.splice(index, 1);
      this.storage.set(this.STORAGE_KEY, result_1);
      var index_2 = this.cities_data.indexOf(city);
      this.cities_data.splice(index_2, 1);
    }
  }

  shareCity(city){
    this.socialShare.share(`Temperatura de ${city.name}: ${city.temp}°. Max: ${city.temp_max}°, Min: ${city.temp_min}°`)
  }

  getAllCities() {
    return this.storage.get(this.STORAGE_KEY);
  }
}
