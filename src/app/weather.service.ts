import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from "rxjs/operators";

@Injectable({providedIn: 'root'})
export class WeatherService {

  // API path
  base_path = 'https://api.openweathermap.org/data/2.5/weather?q=';
  key = '6a51cea0b295e9e35da02c79b321a528';

  constructor(public http: HttpClient) {}

  private extractData(res: Response) {
    let body = res;
    return body || {};
  }

  public getWeather(location: String) {
      return this.http.get(this.base_path + location + "&units=metric"+ "&APPID=" + this.key).pipe(
        map(this.extractData));

  }

}
